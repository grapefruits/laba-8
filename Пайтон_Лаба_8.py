# -*- coding: cp1251 -*-
#��������� ������
success = {
    1: ['IT-21', '��������� ������ �������������', '������ ����', '�� - 60', '�� ����� - 67', 'Python - 100'],
    2: ['IT-21', '��� ��������� �����������', '������ ����', '�� - 78', '�� ����� - 87', 'Python - 98'], 
    3: ['IT-21', '��������� ����� �����������', '������ ����', '�� - 65', '�� ����� - 93', 'Python - 82'],
    4: ['IT-21', '��������� ������ ����������', '������ ����', '�� - 73', '�� ����� - 64', 'Python - 89']
}  #��������� ��������
N = 4  #������� ������� ��������

while True:
  print('0. �������� ������ ��������')
  print('1. ������� ��� ���������� � ���������')
  print('2. ������ �������� �� ������')#��� ���������
  print('3. ����������� ������� ��� �� ��������, �� �������� ��� � ���� ��� ����� �� ��������')#��������� �����
  choice = input('������ ��: ')

  if choice == '1':
    for c in range(1, N + 1):
      for i in range(0, len(success[c])):
        print(success[c][i], end=', ')
      print()

  elif choice == '0':
    print('�����������!')
    break
  #������ �� ���� �� �� ��������
  #��� ��������� - ��������� �������� �� ������

  elif choice == '2':
    N += 1  # �������� ������� ��������
    num = N
    year = int(input('������ ���� ��������: '))
    while year <= 0 or year > 5:
      print('������ ���� �� ���!')
      year = int(input('������ ���� ��������: '))
    name = input('������ ��`� ��������: ')
    second_name = input('������ ������� ��������: ')
    surname = input('������ �������� ��-�������: ')
    group = input('������ ����� ����� ��������: ')

    # ��������� �������� ������� ��� ��������� ������
    grades = {}

    # ��� ������� ��������, ������ ������
    subjects = ['��', '��-�����', 'Python']
    for subject in subjects:
        mark = int(input(f'������ ������ ��� �������� "{subject}": '))
        while mark <= 0:
            print('������ �����, ��� ����� ��� ������� ����!')
            mark = int(input(f'������ ������ ��� �������� "{subject}": '))
        grades[subject] = mark

    # �������� ���������� ��� �������� � ��������
    student_info = {
        'group': group,
        'name': f'{name} {second_name} {surname}',  # �'������� ��'�, ������� � ��-�������
        '����': year,
        '������': grades
    }

    # ��������� ������� success � ������������� ��������� �������
    success[N] = [
        group, f'{name} {second_name} {surname}', str(year) + ' ����',
        f'�� - {grades["��"]}', f'�� ����� - {grades["��-�����"]}', f'Python - {grades["Python"]}'
    ]
    print(f'�������a {num} ������ ������!')
  #������ �� ���� �� �� ��������
  #��������� ����� - ����������� ���������� ���� �� ��������, �� ����� ���, � ���� ��� ����� �� �������
  elif choice == '3':
      subject_to_check = input('������ ������� ��� ��������: ')
      total_marks_subject = 0
      num_students = 0

      # ��������� �������� ���� ���� �� ������� �������� ��� �������� ��������
      for c in range(1, N + 1):
          student_grades = success[c]
          for i in range(3, len(student_grades)):
              subject, mark = student_grades[i].split('-')
              if subject.strip() == subject_to_check and int(mark.strip()) > 0:
                  total_marks_subject += int(mark.strip())
                  num_students += 1

      # ���������� �������� ��� ��� �������� ��������
      average_mark_subject = total_marks_subject / num_students if num_students > 0 else 0

      # �������� ��������, � ���� ��� �� ������� ����� �� ��������
      print(f'�������� ��� �� {subject_to_check}: {average_mark_subject:.2f}')
      print(f'�������� � ����� ���� �� ��������:')
      for c in range(1, N + 1):
          student_grades = success[c]
          for i in range(3, len(student_grades)):
              subject, mark = student_grades[i].split('-')
              if subject.strip() == subject_to_check and int(mark.strip()) > average_mark_subject:
                  print(f'{student_grades[1]} - {subject}: {mark.strip()}')
  # ������ �� ���� �� �� ��������